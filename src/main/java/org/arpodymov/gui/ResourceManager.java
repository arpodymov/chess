package org.arpodymov.gui;

import org.arpodymov.core.piece.Piece;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceType;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author andreypodymov
 */
public class ResourceManager {

    private final String resourcesFolder = "/Users/andreypodymov/JavaProjects/chess/src/main/resources/pieces/";
    private final String piecePicName = "_png_shadow_128px.png";
    private int squareSize;

    private static Map<PieceType, List<String>> piecesUnicodeMap = new HashMap<>();
    private static Map<PieceType, List<ImageIcon>> piecesImageMap = new HashMap<>();

    public ResourceManager(int squareSize) {
        this.squareSize = squareSize;
        piecesUnicodeMap.put(PieceType.ROOK, List.of("♖", "♜"));
        piecesUnicodeMap.put(PieceType.KNIGHT, List.of("♘", "♞"));
        piecesUnicodeMap.put(PieceType.BISHOP, List.of("♗", "♝"));
        piecesUnicodeMap.put(PieceType.QUEEN, List.of("♕", "♛"));
        piecesUnicodeMap.put(PieceType.KING, List.of("♔", "♚"));
        piecesUnicodeMap.put(PieceType.PAWN, List.of("♙", "♟"));

        piecesImageMap.put(PieceType.ROOK,
                List.of(buildImage(PieceType.ROOK, PieceColor.WHITE),
                        buildImage(PieceType.ROOK, PieceColor.BLACK)
                )
        );
        piecesImageMap.put(PieceType.KNIGHT,
                List.of(buildImage(PieceType.KNIGHT, PieceColor.WHITE),
                        buildImage(PieceType.KNIGHT, PieceColor.BLACK)
                )
        );
        piecesImageMap.put(PieceType.BISHOP,
                List.of(buildImage(PieceType.BISHOP, PieceColor.WHITE),
                        buildImage(PieceType.BISHOP, PieceColor.BLACK)
                )
        );
        piecesImageMap.put(PieceType.QUEEN,
                List.of(buildImage(PieceType.QUEEN, PieceColor.WHITE),
                        buildImage(PieceType.QUEEN, PieceColor.BLACK)
                )
        );
        piecesImageMap.put(PieceType.KING,
                List.of(buildImage(PieceType.KING, PieceColor.WHITE),
                        buildImage(PieceType.KING, PieceColor.BLACK)
                )
        );
        piecesImageMap.put(PieceType.PAWN,
                List.of(buildImage(PieceType.PAWN, PieceColor.WHITE),
                        buildImage(PieceType.PAWN, PieceColor.BLACK)
                )
        );
    }

    public ImageIcon getImage(Piece piece) {
        if (piece.getColor().equals(PieceColor.WHITE)) {
            return piecesImageMap.get(piece.getType()).get(0);
        }
        return piecesImageMap.get(piece.getType()).get(1);
    }

    public String getUnicodeSymbol(Piece piece) {
        List<String> list = piecesUnicodeMap.get(piece.getType());
        if (piece.getColor().equals(PieceColor.WHITE)) {
            return list.get(0);
        }
        return list.get(1);
    }

    private ImageIcon buildImage(PieceType type, PieceColor color) {
        StringBuilder name = new StringBuilder();
        name.append(resourcesFolder)
                .append(String.valueOf(color.name().charAt(0)).toLowerCase()+"_")
                .append(type.toString().toLowerCase())
                .append(piecePicName);

        ImageIcon imageIcon = new ImageIcon(name.toString());
        Image image = imageIcon.getImage();
        Image newimg = image.getScaledInstance(squareSize /10, squareSize /10,  java.awt.Image.SCALE_SMOOTH);
        imageIcon = new ImageIcon(newimg);
        return imageIcon;
    }
}
