package org.arpodymov.gui;

import com.google.common.eventbus.Subscribe;
import org.arpodymov.core.board.Board;
import org.arpodymov.core.board.BoardNewStateEvent;
import org.arpodymov.core.board.BoardSquare;
import org.arpodymov.core.piece.PieceColor;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardPanel extends JPanel {

    private final int SQUARE_SIZE = 800;
    private final Color LIGHT_SQUARE_COLOR = new Color(255,102,102);
    private final Color BLACK_SQUARE_COLOR = new Color(32, 96, 206);

    private ResourceManager resourceManager;
    private Board board;
    private Map<JPanel,JLabel> piecesMap;
    private JLayeredPane layeredPane;
    private JPanel chessBoardPanel;

    public BoardPanel(Board board) {
        this.board = board;
        this.piecesMap = new HashMap<>();
        board.subsribeToMoves(this);
        Dimension boardSize = new Dimension(SQUARE_SIZE, SQUARE_SIZE);
        resourceManager = new ResourceManager(SQUARE_SIZE);
        layeredPane = new JLayeredPane();
        this.add(layeredPane);
        layeredPane.setPreferredSize(boardSize);
        chessBoardPanel = new JPanel();
        layeredPane.add(chessBoardPanel, JLayeredPane.DEFAULT_LAYER);
        chessBoardPanel.setLayout( new GridLayout(board.getBoardSize(), board.getBoardSize()) );
        chessBoardPanel.setPreferredSize( boardSize );
        chessBoardPanel.setBounds(0, 0, boardSize.width, boardSize.height);
        for (int i = 0; i < board.getBoardSize() * board.getBoardSize(); i++) {
            JPanel square = new JPanel(new BorderLayout());
            chessBoardPanel.add(square);
            int row = (i / board.getBoardSize()) % 2;
            if (row == 0) {
                square.setBackground(i % 2 == 0 ? LIGHT_SQUARE_COLOR : BLACK_SQUARE_COLOR);
            }
            else {
                square.setBackground(i % 2 == 0 ? BLACK_SQUARE_COLOR : LIGHT_SQUARE_COLOR);
            }
        }
        SwingUtilities.invokeLater(() -> repaintBoard(board.getPieces(PieceColor.WHITE), board.getPieces(PieceColor.BLACK)));
    }

    @Subscribe
    public void subscribeState(BoardNewStateEvent stateEvent) {
        SwingUtilities.invokeLater(() -> repaintBoard(stateEvent.getWhiteCells(), stateEvent.getBlackCells()));
    }

    private void repaintBoard(List<BoardSquare> whiteCells, List<BoardSquare> blackCells) {
        for (Map.Entry<JPanel, JLabel> entry : this.piecesMap.entrySet()) {
            JPanel key = entry.getKey();
            JLabel value = entry.getValue();
            key.remove(value);
        }
        this.piecesMap.clear();

        List<BoardSquare> allCells = new ArrayList<>();
        allCells.addAll(whiteCells);
        allCells.addAll(blackCells);
        for (BoardSquare cell : allCells) {
            if (cell.getPiece() == null) {
                continue;
            }
            JLabel piece = new JLabel(resourceManager.getImage(cell.getPiece()));
            JPanel panel = (JPanel) this.chessBoardPanel.getComponent((board.getBoardSize()*board.getBoardSize()-1)-(board.getBoardSize()-1-cell.getPosition().getX() + cell.getPosition().getY() * board.getBoardSize()));
            panel.add(piece);
            this.piecesMap.put(panel, piece);
        }
        SwingUtilities.updateComponentTreeUI(this.chessBoardPanel);
    }
}