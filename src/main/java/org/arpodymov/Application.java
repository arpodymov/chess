package org.arpodymov;

import org.arpodymov.core.board.Board;
import org.arpodymov.game.Game;
import org.arpodymov.game.StandartPlacementRule;
import org.arpodymov.gui.BoardPanel;

import javax.swing.*;
import java.awt.*;

/**
 * @author andreypodymov
 */
public class Application {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Chess");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        JPanel panel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(boxLayout);

        JButton startBtn = new JButton("Start Random Game :)");
        Board board = new Board(new StandartPlacementRule());
        startBtn.addActionListener(e -> {
            startBtn.setEnabled(false);
            startGame(board);
        });

        panel.add(startBtn);
        BoardPanel boardFrame = new BoardPanel(board);
        panel.add(boardFrame);
        contentPane.add(panel, BorderLayout.SOUTH);
        frame.pack();
        centreWindow(frame);
        frame.setVisible(true);
    }

    private static void startGame(Board board) {
        Game game = new Game(board);
        game.startInfiniteGame(10);
    }

    private static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }
}
