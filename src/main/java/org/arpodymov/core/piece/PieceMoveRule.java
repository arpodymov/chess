package org.arpodymov.core.piece;

import org.arpodymov.core.board.Point;

import java.util.Set;

/**
 * @author andreypodymov
 */
public interface PieceMoveRule {
    boolean isLongRange();
    Set<Point> getPoints();
}
