package org.arpodymov.core.piece;

import org.arpodymov.core.piece.impl.*;

/**
 * @author andreypodymov
 */
public class PieceSet {
    public static Rook whiteRook = new Rook(PieceColor.WHITE);
    public static Rook blackRook = new Rook(PieceColor.BLACK);

    public static Knight whiteKnight = new Knight(PieceColor.WHITE);
    public static Knight blackKnight = new Knight(PieceColor.BLACK);

    public static Bishop whiteBishop = new Bishop(PieceColor.WHITE);
    public static Bishop blackBishop = new Bishop(PieceColor.BLACK);

    public static Queen whiteQueen = new Queen(PieceColor.WHITE);
    public static Queen blackQueen = new Queen(PieceColor.BLACK);

    public static King whiteKing = new King(PieceColor.WHITE);
    public static King blackKing = new King(PieceColor.BLACK);

    public static Pawn whitePawn = new Pawn(PieceColor.WHITE);
    public static Pawn blackPawn = new Pawn(PieceColor.BLACK);
}
