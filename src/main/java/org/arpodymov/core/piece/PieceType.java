package org.arpodymov.core.piece;

/**
 * @author andreypodymov
 */
public enum PieceType {
    PAWN,
    ROOK,
    KNIGHT,
    BISHOP,
    QUEEN,
    KING
}
