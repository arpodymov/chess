package org.arpodymov.core.piece;

/**
 * @author andreypodymov
 */
public enum PieceColor {
    WHITE,
    BLACK
}
