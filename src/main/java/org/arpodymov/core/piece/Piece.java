package org.arpodymov.core.piece;

import lombok.Data;
import lombok.ToString;

/**
 * @author andreypodymov
 */
@Data
@ToString
public abstract class Piece {
    protected boolean isMoved;
    protected PieceColor color;

    public abstract PieceType getType();
    public abstract PieceMoveRule getMoveRule();

    public Piece(PieceColor color) {
        this.color = color;
        this.isMoved = false;
    }
}
