package org.arpodymov.core.piece.impl;

import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.Piece;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceMoveRule;
import org.arpodymov.core.piece.PieceType;

import java.util.HashSet;
import java.util.Set;

/**
 * @author andreypodymov
 */
public class Knight extends Piece {
    public Knight(PieceColor color) {
        super(color);
    }

    @Override
    public PieceType getType() {
        return PieceType.KNIGHT;
    }
    @Override
    public PieceMoveRule getMoveRule() {
        return new PieceMoveRule() {
            @Override
            public boolean isLongRange() {
                return false;
            }

            @Override
            public Set<Point> getPoints() {
                Set<Point> points = new HashSet<>();
                points.add(Point.of(1, 2));
                points.add(Point.of(2, 1));
                points.add(Point.of(2, -1));
                points.add(Point.of(1, -2));
                points.add(Point.of(-1, -2));
                points.add(Point.of(-2, -1));
                points.add(Point.of(-2, 1));
                points.add(Point.of(-1, 2));
                return points;
            }
        };
    }
}
