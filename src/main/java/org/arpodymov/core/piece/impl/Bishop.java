package org.arpodymov.core.piece.impl;

import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.Piece;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceMoveRule;
import org.arpodymov.core.piece.PieceType;

import java.util.Set;

/**
 * @author andreypodymov
 */
public class Bishop extends Piece {
    public Bishop(PieceColor color) {
        super(color);
    }

    @Override
    public PieceType getType() {
        return PieceType.BISHOP;
    }
    @Override
    public PieceMoveRule getMoveRule() {
       return new PieceMoveRule() {
           @Override
           public boolean isLongRange() {
               return true;
           }

           @Override
           public Set<Point> getPoints() {
               return Set.of(Point.of(1,1),
                       Point.of(1, -1),
                       Point.of(-1, -1),
                       Point.of(-1, 1)
               );
           }
       };
    }
}
