package org.arpodymov.core.piece.impl;

import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.Piece;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceMoveRule;
import org.arpodymov.core.piece.PieceType;

import java.util.HashSet;
import java.util.Set;

/**
 * @author andreypodymov
 */
public class Pawn extends Piece {
    public Pawn(PieceColor color) {
        super(color);
    }

    @Override
    public PieceType getType() {
        return PieceType.PAWN;
    }
    @Override
    public PieceMoveRule getMoveRule() {
        return new PieceMoveRule() {
            @Override
            public boolean isLongRange() {
                return false;
            }

            @Override
            public Set<Point> getPoints() {
                Set<Point> points = new HashSet<>();
                points.add(Point.of(0, 1));
                if (!isMoved) {
                    points.add(Point.of(0, 2));
                }
                return points;
            }
        };
    }
}
