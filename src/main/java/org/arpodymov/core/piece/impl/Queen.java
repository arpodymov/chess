package org.arpodymov.core.piece.impl;

import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.*;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author andreypodymov
 */
public class Queen extends Piece {
    public Queen(PieceColor color) {
        super(color);
    }

    @Override
    public PieceType getType() {
        return PieceType.QUEEN;
    }
    @Override
    public PieceMoveRule getMoveRule() {
       return new PieceMoveRule() {
           @Override
           public boolean isLongRange() {
               return true;
           }

           @Override
           public Set<Point> getPoints() {
               Set<Point> bishop = PieceSet.whiteBishop.getMoveRule().getPoints();
               Set<Point> rook = PieceSet.whiteRook.getMoveRule().getPoints();
               return Stream.of(bishop, rook)
                      .flatMap(Collection::stream)
                      .collect(Collectors.toSet());
           }
       };
    }
}
