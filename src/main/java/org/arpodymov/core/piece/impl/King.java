package org.arpodymov.core.piece.impl;

import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.*;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author andreypodymov
 */
public class King extends Piece {
    public King(PieceColor color) {
        super(color);
    }

    @Override
    public PieceType getType() {
        return PieceType.KING;
    }
    @Override
    public PieceMoveRule getMoveRule() {
       return new PieceMoveRule() {
           @Override
           public boolean isLongRange() {
               return false;
           }

           @Override
           public Set<Point> getPoints() {
               Set<Point> bishop = PieceSet.whiteBishop.getMoveRule().getPoints();
               Set<Point> rook = PieceSet.whiteRook.getMoveRule().getPoints();
               return Stream.of(bishop, rook)
                      .flatMap(Collection::stream)
                      .collect(Collectors.toSet());
           }
       };
    }
}
