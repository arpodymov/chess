package org.arpodymov.core.board;

import com.google.common.eventbus.EventBus;
import lombok.ToString;
import org.arpodymov.core.piece.Piece;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceMoveRule;
import org.arpodymov.core.piece.PieceType;
import org.arpodymov.core.piece.impl.*;
import org.arpodymov.game.InitialPlacementRule;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author andreypodymov
 */
@ToString
public class Board {
    private final int BOARD_SIZE = 8;
    private EventBus bus;
    private InitialPlacementRule placementRule;
    private Map<Point, BoardSquare> cells;

    public Board(InitialPlacementRule placementRule) {
        this.bus = new EventBus();
        this.placementRule = placementRule;
        generateCells();
        placePieces();
    }

    public List<BoardSquare> getPieces(PieceColor color) {
        return cells.values().stream().filter(t->t.getPiece()!=null && t.getPiece().getColor().equals(color)).collect(Collectors.toList());
    }

    public void moveRandom(PieceColor color) {
        List<BoardSquare> cells = getPieces(color);
        if (cells.isEmpty()) {
            return;
        }
        Random random = new Random();
        BoardSquare cell = cells.get(random.nextInt(cells.size()));
        Piece piece = cell.getPiece();
        List<Point> availableMoves = getAvailableMoves(piece, cell.getPosition());
        if (!availableMoves.isEmpty()) {
            movePiece(piece, cell, this.cells.get(availableMoves.get(random.nextInt(availableMoves.size()))));
        }
        bus.post(new BoardNewStateEvent(getPieces(PieceColor.WHITE), getPieces(PieceColor.BLACK)));
    }

    public void subsribeToMoves(Object obj) {
        bus.register(obj);
    }

    public List<Point> getAvailableMoves(Piece piece, Point position) {
        List<Point> moves = new ArrayList<>();
        PieceMoveRule rule = piece.getMoveRule();
        Set<Point> directions = rule.getPoints();
        if (!rule.isLongRange()) {
            for (Point direction : directions) {
                int x, y;
                if (piece.getColor().equals(PieceColor.WHITE)) {
                    x = position.getX() + direction.getX();
                    y = position.getY() + direction.getY();
                }
                else {
                    x = position.getX() - direction.getX();
                    y = position.getY() - direction.getY();
                }
                if ((x < 0 || x >= BOARD_SIZE)|| (y<0||y>= BOARD_SIZE)) {
                    continue;
                }
                else {
                    moves.add(Point.of(x, y));
                }
            }
        }
        else {
            for (Point direction : directions) {
                Point positionNow = Point.of(position.getX(), position.getY());
                while (true) {
                    int x, y;
                    if (piece.getColor().equals(PieceColor.WHITE)) {
                        x = positionNow.getX() + direction.getX();
                        y = positionNow.getY() + direction.getY();
                    }
                    else {
                        x = positionNow.getX() - direction.getX();
                        y = positionNow.getY() - direction.getY();
                    }
                    if ((x < 0 || x >= BOARD_SIZE)||(y<0||y>= BOARD_SIZE)) {
                        break;
                    }
                    Point result = Point.of(x, y);
                    moves.add(result);
                    positionNow.setX(x);
                    positionNow.setY(y);
                }
            }
        }
        return moves;
    }


    private void generateCells() {
        cells = new HashMap<>();
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                Point point = Point.of(i, j);
                BoardSquare cell = new BoardSquare(Point.of(i, j));
                cells.put(point, cell);
            }
        }
    }

    public void placePieces() {
        placeRooks();
        placeKnights();
        placeBishops();
        placeQueens();
        placeKings();
        placePawns();
    }

    private void placeRooks() {
        Rook white1 = new Rook(PieceColor.WHITE);
        Rook white2 = new Rook(PieceColor.WHITE);
        List<Point> pointsWhite = placementRule.placement(PieceColor.WHITE).get(PieceType.ROOK);
        placePiece(cells.get(pointsWhite.get(0)), white1);
        placePiece(cells.get(pointsWhite.get(1)), white2);

        Rook black1 = new Rook(PieceColor.BLACK);
        Rook black2 = new Rook(PieceColor.BLACK);
        List<Point> pointsBlack = placementRule.placement(PieceColor.BLACK).get(PieceType.ROOK);
        placePiece(cells.get(pointsBlack.get(0)), black1);
        placePiece(cells.get(pointsBlack.get(1)), black2);
    }

    private void placeKnights() {
        Knight white1 = new Knight(PieceColor.WHITE);
        Knight white2 = new Knight(PieceColor.WHITE);
        List<Point> pointsWhite = placementRule.placement(PieceColor.WHITE).get(PieceType.KNIGHT);
        placePiece(cells.get(pointsWhite.get(0)), white1);
        placePiece(cells.get(pointsWhite.get(1)), white2);

        Knight black1 = new Knight(PieceColor.BLACK);
        Knight black2 = new Knight(PieceColor.BLACK);
        List<Point> pointsBlack = placementRule.placement(PieceColor.BLACK).get(PieceType.KNIGHT);
        placePiece(cells.get(pointsBlack.get(0)), black1);
        placePiece(cells.get(pointsBlack.get(1)), black2);
    }

    private void placeBishops() {
        Bishop white1 = new Bishop(PieceColor.WHITE);
        Bishop white2 = new Bishop(PieceColor.WHITE);
        List<Point> pointsWhite = placementRule.placement(PieceColor.WHITE).get(PieceType.BISHOP);
        placePiece(cells.get(pointsWhite.get(0)), white1);
        placePiece(cells.get(pointsWhite.get(1)), white2);

        Bishop black1 = new Bishop(PieceColor.BLACK);
        Bishop black2 = new Bishop(PieceColor.BLACK);
        List<Point> pointsBlack = placementRule.placement(PieceColor.BLACK).get(PieceType.BISHOP);
        placePiece(cells.get(pointsBlack.get(0)), black1);
        placePiece(cells.get(pointsBlack.get(1)), black2);
    }

    private void placeQueens() {
        Queen white1 = new Queen(PieceColor.WHITE);
        List<Point> pointsWhite = placementRule.placement(PieceColor.WHITE).get(PieceType.QUEEN);
        placePiece(cells.get(pointsWhite.get(0)), white1);
        Queen black = new Queen(PieceColor.BLACK);
        List<Point> pointsBlack = placementRule.placement(PieceColor.BLACK).get(PieceType.QUEEN);
        placePiece(cells.get(pointsBlack.get(0)), black);
    }

    private void placeKings() {
        King white1 = new King(PieceColor.WHITE);
        List<Point> pointsWhite = placementRule.placement(PieceColor.WHITE).get(PieceType.KING);
        placePiece(cells.get(pointsWhite.get(0)), white1);

        King black = new King(PieceColor.BLACK);
        List<Point> pointsBlack = placementRule.placement(PieceColor.BLACK).get(PieceType.KING);
        placePiece(cells.get(pointsBlack.get(0)), black);
    }

    private void placePawns() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            Pawn white = new Pawn(PieceColor.WHITE);
            List<Point> pointsWhite = placementRule.placement(PieceColor.WHITE).get(PieceType.PAWN);
            placePiece(cells.get(pointsWhite.get(i)), white);
        }

        for (int i = 0; i < BOARD_SIZE; i++) {
            Pawn black = new Pawn(PieceColor.BLACK);
            List<Point> pointsBlack = placementRule.placement(PieceColor.BLACK).get(PieceType.PAWN);
            placePiece(cells.get(pointsBlack.get(i)), black);
        }
    }

    private void placePiece(BoardSquare cell, Piece piece) {
        cell.setPiece(piece);
    }

    private void movePiece(Piece piece, BoardSquare cell, BoardSquare dest) {
        piece.setMoved(true);
        cell.setPiece(null);
        if (piece.getType().equals(PieceType.PAWN) && (dest.getPosition().getY() == 0 || dest.getPosition().getY() == 7)) {
            dest.setPiece(new Queen(piece.getColor()));
        }
        else {
            dest.setPiece(piece);
        }
    }
    
    public int getBoardSize() {
        return BOARD_SIZE;
    }
}
