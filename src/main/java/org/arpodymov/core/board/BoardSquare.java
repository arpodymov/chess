package org.arpodymov.core.board;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.arpodymov.core.piece.Piece;

/**
 * @author andreypodymov
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BoardSquare {
    private Point position;
    private Piece piece;

    public BoardSquare(Point position) {
        this.position = position;
    }

    public boolean isOccupied() {
        return piece != null;
    }
}
