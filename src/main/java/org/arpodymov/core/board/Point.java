package org.arpodymov.core.board;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author andreypodymov
 */
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Point {
    private int x;
    private int y;

    public static Point of(int x, int y) {
        return new Point(x,y);
    }
}
