package org.arpodymov.core.board;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author andreypodymov
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BoardNewStateEvent {
    private List<BoardSquare> whiteCells;
    private List<BoardSquare> blackCells;
}
