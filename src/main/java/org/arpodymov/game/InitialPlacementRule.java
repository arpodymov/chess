package org.arpodymov.game;

import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceType;

import java.util.List;
import java.util.Map;

/**
 * @author andreypodymov
 */
public interface InitialPlacementRule {
    Map<PieceType, List<Point>> placement(PieceColor color);
}
