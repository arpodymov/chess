package org.arpodymov.game;

import lombok.Getter;
import org.arpodymov.core.board.Point;
import org.arpodymov.core.piece.PieceColor;
import org.arpodymov.core.piece.PieceType;

import java.util.*;

/**
 * @author andreypodymov
 */
public class StandartPlacementRule implements InitialPlacementRule {

    @Getter
    private Map<PieceType, List<Point>> map = new HashMap<>();

    @Override
    public Map<PieceType, List<Point>> placement(PieceColor color) {
        Map<PieceType, List<Point>> map = new HashMap<>();
        if (color.equals(PieceColor.WHITE)) {
            map.put(PieceType.ROOK, Arrays.asList(Point.of(0, 0), Point.of(7,0)));
            map.put(PieceType.KNIGHT, Arrays.asList(Point.of(1, 0), Point.of(6,0)));
            map.put(PieceType.BISHOP, Arrays.asList(Point.of(2, 0), Point.of(5,0)));
            map.put(PieceType.QUEEN, Arrays.asList(Point.of(3, 0)));
            map.put(PieceType.KING, Arrays.asList(Point.of(4, 0)));
            List<Point> pawnsList = new ArrayList<>();
            for (int i = 0; i < 8; i++) {
                pawnsList.add(Point.of(i, 1));
            }
            map.put(PieceType.PAWN, pawnsList);
        }
        else {
            map.put(PieceType.ROOK, Arrays.asList(Point.of(0, 7), Point.of(7,7)));
            map.put(PieceType.KNIGHT, Arrays.asList(Point.of(1, 7), Point.of(6,7)));
            map.put(PieceType.BISHOP, Arrays.asList(Point.of(2, 7), Point.of(5,7)));
            map.put(PieceType.QUEEN, Arrays.asList(Point.of(3, 7)));
            map.put(PieceType.KING, Arrays.asList(Point.of(4, 7)));
            List<Point> pawnsList = new ArrayList<>();
            for (int i = 0; i < 8; i++) {
                pawnsList.add(Point.of(i, 6));
            }
            map.put(PieceType.PAWN, pawnsList);
        }
        return map;

    }
}
