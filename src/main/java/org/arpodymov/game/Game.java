package org.arpodymov.game;

import org.arpodymov.core.board.Board;
import org.arpodymov.core.piece.PieceColor;

/**
 * @author andreypodymov
 */
public class Game {
    private Board board;
    private boolean nextMovesWhite;

    public Game(Board board) {
        this.nextMovesWhite = true;
        this.board = board;
    }

    public void startInfiniteGame(int sleep) {
        new Thread(() -> {
            while (true) {
                if (nextMovesWhite) {
                    board.moveRandom(PieceColor.WHITE);
                }
                else {
                    board.moveRandom(PieceColor.BLACK);
                }
                nextMovesWhite = !nextMovesWhite;
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();
    }
}
